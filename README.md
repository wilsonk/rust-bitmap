# rust-sel4

[![Crates.io](https://img.shields.io/crates/v/bitmap.svg?style=flat-square)](https://crates.io/crates/bitmap)

[Documentation](https://doc.robigalia.org/bitmap)

A simple datastructure: an array of values whose width in bits is not necessarily a
multiple of 8.

## Status

Complete.
